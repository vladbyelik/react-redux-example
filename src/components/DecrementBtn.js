import React from 'react';
import { connect } from 'react-redux';

const decrementAction = {
  type: 'DEC'
}

const randomAction = (value) => {
  return {
    type: 'RND',
    payload: value
  }
}

class DecrementBtn extends React.Component {
  render() {
    return <p>
      <button onClick={() => this.props.dispatch(decrementAction)}>
        -
      </button>

      <button onClick={() => this.props.dispatch(randomAction( Math.round(Math.random() * 10)))}>
        RND
      </button>
    </p>
  }
}

const mapStateToProps = (state) => {
  return {
    reduxStore: state
  }
}

export default connect(mapStateToProps)(DecrementBtn);