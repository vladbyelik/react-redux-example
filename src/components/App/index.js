import React from 'react';
import Clock from '../Clock';
import DecrementBtn from '../DecrementBtn';
import IncrementBtn from '../IncrementBtn';
import LifecycleExample from '../LifecycleExample';
import './App.css';

class App extends React.Component {

  state = {
    isHidden: false
  }
  
  render() {
    return (
      <div className="App">
        <button onClick={() => this.setState({ isHidden: !this.state.isHidden })}>
          Show/Hide
        </button>

        {/* {this.state.isHidden ? <h4>Hidden </h4>: <LifecycleExample />} */}
        {/* {this.state.isHidden ? <h4>Hidden </h4>: <Clock />} */}

        <IncrementBtn />
        <DecrementBtn />

        <LifecycleExample />
      </div>
    );
  }
}

export default App;
