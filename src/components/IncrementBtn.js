import React from 'react';
import { connect } from 'react-redux';

const incrementAction = {
  type: 'INC'
}

class IncrementBtn extends React.Component {
  render() {

    // console.log(this.props.dispatch());

    return <p>
      <button onClick={() => this.props.dispatch(incrementAction)}>
        +
      </button>
    </p>
  }
}

const mapStateToProps = (state) => {
  return {
    reduxStore: state
  }
}

export default connect(mapStateToProps)(IncrementBtn);