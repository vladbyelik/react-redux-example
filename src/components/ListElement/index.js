import React from 'react';

const ListElement = ({ title }) => {
  return (
    <li>
      <p>
        {title}
      </p>
    </li>
  )
}

export default ListElement;